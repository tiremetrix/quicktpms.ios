//
//  DBModule.m
//  alligator-iphone
//
//  Created by Scott Holliday on 10/26/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "DBModule.h"

@implementation DBModule

+ (id)sharedInstance{
    static DBModule *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (id)init{
    if(self = [super init]){
        NSFileManager *mgrFile = [NSFileManager defaultManager];
        NSString *strTemp = [[NSBundle mainBundle] pathForResource:@"tpms" ofType:@"sqlite"];
        NSString *strDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *strDBPath = [strDir stringByAppendingPathComponent:@"tpms.sqlite"];
        if(![mgrFile fileExistsAtPath:strDBPath]){
            [mgrFile copyItemAtPath:strTemp toPath:strDBPath error:nil];
        }
        sqlite3_open([strDBPath UTF8String], &db);
        
    }
    return self;
}

- (NSArray *)getAllYears{
    NSMutableArray *years = [[NSMutableArray alloc] init];
    [years addObject:[NSNumber numberWithInt:0]];
    NSString *strSQL = @"SELECT DISTINCT Year FROM ymm ORDER BY Year ASC";
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            NSNumber *year = [NSNumber numberWithInt:sqlite3_column_int(cmpStmt, 0)];
            [years addObject:year];
        }
    }
    sqlite3_finalize(cmpStmt);
    return years;
}

- (NSArray *)getMakesByYear:(NSString *)year{
    NSMutableArray *makes = [[NSMutableArray alloc] init];
    NSString *strSQL = [NSString stringWithFormat:@"SELECT DISTINCT m.ID, m.Make FROM makes m JOIN ymm y ON m.ID = y.MakeID WHERE y.Year = %@ ORDER BY Make ASC", year];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            Make *make = [[Make alloc] init];
            [make setIdentifier:sqlite3_column_int(cmpStmt, 0)];
            [make setName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 1)]];
            [makes addObject:make];
        }
    }
    sqlite3_finalize(cmpStmt);
    return makes;
}

- (NSArray *)getModelsByYearAndMake:(NSString *)year mke:(Make *)make{
    NSMutableArray *models = [[NSMutableArray alloc] init];
    NSString *strSQL = [NSString stringWithFormat:@"SELECT DISTINCT m.ID, m.Model FROM models m JOIN ymm y ON m.ID = y.ModelID WHERE y.Year = %@ AND y.MakeID = %d ORDER BY m.Model ASC", year, [make identifier]];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            Model *model = [[Model alloc] init];
            [model setIdentifier:sqlite3_column_int(cmpStmt, 0)];
            [model setName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 1)]];
            [models addObject:model];
        }
    }
    sqlite3_finalize(cmpStmt);
    return models;
}

@end
