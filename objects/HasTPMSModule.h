//
//  HasTPMSModule.h
//  quicktpms-iphone
//
//  Created by Scott Holliday on 11/20/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HasTPMSModule : NSObject

+ (id)sharedInstance;
- (void)hasTPMS:(BOOL)byVIN;

@end
