//
//  HasTPMSModule.m
//  quicktpms-iphone
//
//  Created by Scott Holliday on 11/20/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "HasTPMSModule.h"

@implementation HasTPMSModule

+ (id)sharedInstance{
    static HasTPMSModule *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)hasTPMS:(BOOL)byVIN{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://admin.tpmsmanager.com/CheckTPMS.svc?wsdl"]];
    [request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"http://tempuri.org/ICheckTPMS/GetTPMS" forHTTPHeaderField:@"SOAPAction"];
    [request setHTTPMethod:@"POST"];
    NSString *soapMsg = @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:has=\"http://schemas.datacontract.org/2004/07/HasTPMS\"><soapenv:Header/><soapenv:Body><tem:GetTPMS><tem:t><has:AccessKey>tiremetrix</has:AccessKey>%@</tem:t></tem:GetTPMS></soapenv:Body></soapenv:Envelope>";
    if(byVIN){
        NSString *vin = [[NSUserDefaults standardUserDefaults] objectForKey:@"vin"];
        soapMsg = [NSString stringWithFormat:soapMsg, [NSString stringWithFormat:@"<has:VIN>%@</has:VIN>", vin]];
    }else{
        NSString *year = [[NSUserDefaults standardUserDefaults] objectForKey:@"year"];
        NSString *make = [[NSUserDefaults standardUserDefaults] objectForKey:@"make"];
        NSString *model = [[NSUserDefaults standardUserDefaults] objectForKey:@"model"];
        soapMsg = [NSString stringWithFormat:soapMsg, [NSString stringWithFormat:@"<has:Make>%@</has:Make><has:Model>%@</has:Model><has:Year>%@</has:Year>", make, model, year]];
    }
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMsg length]];
    [request setHTTPBody: [soapMsg dataUsingEncoding:NSUTF8StringEncoding]];
    [request addValue:msgLength forHTTPHeaderField:@"Content-Length"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        NSDictionary *dicStatus;
        if(error){
            dicStatus = @{@"status":@"500"};
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*)response;
        if(httpResp.statusCode != 200){
            dicStatus = @{@"status":[NSString stringWithFormat:@"%ld", (long)httpResp.statusCode]};
        }else{
            dicStatus = @{@"status":[NSString stringWithFormat:@"%ld", (long)httpResp.statusCode], @"data":data};
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RetrievalResult" object:dicStatus];
    }];
    [task resume];
}

@end
