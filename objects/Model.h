//
//  Model.h
//  alligator-iphone
//
//  Created by Scott Holliday on 10/26/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject

@property (assign) int identifier;
@property (nonatomic, strong) NSString *name;

@end
