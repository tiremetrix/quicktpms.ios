//
//  RetrievalModule.h
//  quicktpms-ipad
//
//  Created by Scott Holliday on 11/4/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RetrievalModule : NSObject

+ (id)sharedInstance;
- (void)getTPMS:(BOOL)byVIN;

@end
