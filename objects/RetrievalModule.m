//
//  RetrievalModule.m
//  quicktpms-ipad
//
//  Created by Scott Holliday on 11/4/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "RetrievalModule.h"

@implementation RetrievalModule

+ (id)sharedInstance{
    static RetrievalModule *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)getTPMS:(BOOL)byVIN{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://id.tiremetrix.com/token"]];
    [request setHTTPMethod:@"POST"];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    NSString *authString;
    if(byVIN){
        NSString *vin = [[NSUserDefaults standardUserDefaults] objectForKey:@"vin"];
        authString = [NSString stringWithFormat:@"{\"vin\":\"%@\",\"token\":\"%@\"}", vin, token];
    }else{
        NSString *year = [[NSUserDefaults standardUserDefaults] objectForKey:@"year"];
        NSString *make = [[NSUserDefaults standardUserDefaults] objectForKey:@"make"];
        NSString *model = [[NSUserDefaults standardUserDefaults] objectForKey:@"model"];
        authString = [NSString stringWithFormat:@"{\"year\":\"%@\",\"make\":\"%@\",\"model\":\"%@\",\"token\":\"%@\"}", year, make, model, token];
    }
    [request addValue:@"application/json;" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[authString length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:[authString dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            NSDictionary *dicStatus = @{@"status":@"500"};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RetrievalResult" object:dicStatus];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*)response;
        if(httpResp.statusCode != 200){
            NSDictionary *dicStatus = @{@"status":[NSString stringWithFormat:@"%ld", (long)httpResp.statusCode]};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RetrievalResult" object:dicStatus];
        }else{
            //read data
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSDictionary *dicStatus = @{@"status":@"200", @"vehicleData":dic};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RetrievalResult" object:dicStatus];
        }
    }];
    [task resume];
}

@end
