//
//  ServiceKit.h
//  alligator-iphone
//
//  Created by Scott Holliday on 10/26/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceKit : NSObject

@property (nonatomic, strong) NSString *name;
@property (assign) int frequency;

@end
