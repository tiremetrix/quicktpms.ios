//
//  TokenModule.m
//  quicktpms-iphone
//
//  Created by Scott Holliday on 11/3/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "TokenModule.h"

@implementation TokenModule

+ (id)sharedInstance{
    static TokenModule *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)getToken{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"exp"]){
        NSDate *expDate = [NSDate dateWithTimeIntervalSince1970:[[[NSUserDefaults standardUserDefaults] objectForKey:@"exp"] intValue]];
        if([expDate timeIntervalSinceNow] <= 0){
            [self getNewToken];
        }else{
            NSDictionary *dicStatus = @{@"status":@"200"};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TokenResult" object:dicStatus];
        }
    }else{
        [self getNewToken];
    }
}

- (void)getNewToken{
    NSString *nonce = [self randomStringWithLength:10];
    const char *secret  = [@"uY-2WJ9K$bG5RZ#hX5YY#$RSVu4$8my$" cStringUsingEncoding:NSASCIIStringEncoding];
    NSString *accessKey = @"037CEDD2-2536-4B99-9B38-F7C86A8EF7B6";
    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSData *deviceInfo = [@"{\"profile\":\"tm.huf.mobile.ios\"}" dataUsingEncoding:NSUTF8StringEncoding];
    const char *toBeHashed = [[NSString stringWithFormat:@"%@+%@+%@", accessKey, nonce, deviceId] cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char hmac[CC_SHA512_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, secret, strlen(secret), toBeHashed, strlen(toBeHashed), hmac);
    NSData *hash = [[NSData alloc] initWithBytes:hmac length:sizeof(hmac)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://id.tiremetrix.com/token"]];
    [request setHTTPMethod:@"POST"];
    NSString *authString = [NSString stringWithFormat:@"{\"nonce\":\"%@\",\"accessKey\":\"%@\",\"id\":\"%@\",\"info\":\"%@\",\"hash\":\"%@\"}", nonce, accessKey, deviceId, [deviceInfo base64EncodedStringWithOptions:0], [hash base64EncodedStringWithOptions:0]];
    [request addValue:@"application/json;" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[authString length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:[authString dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            NSDictionary *dicStatus = @{@"status":@"500"};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TokenResult" object:dicStatus];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*)response;
        if(httpResp.statusCode != 200){
            NSDictionary *dicStatus = @{@"status":[NSString stringWithFormat:@"%ld", (long)httpResp.statusCode]};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TokenResult" object:dicStatus];
        }else{
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSString *jwt = [dic objectForKey:@"accessToken"];
            NSArray *jwtParts = [jwt componentsSeparatedByString:@"."];
            NSData *dataBody = [[NSData alloc] initWithBase64EncodedString:[jwtParts objectAtIndex:1] options:0];
            NSDictionary *dicBody = [NSJSONSerialization JSONObjectWithData:dataBody options:NSJSONReadingAllowFragments error:nil];
            [[NSUserDefaults standardUserDefaults] setObject:[dicBody objectForKey:@"aud"] forKey:@"aud"];
            [[NSUserDefaults standardUserDefaults] setObject:[dicBody objectForKey:@"exp"] forKey:@"exp"];
            [[NSUserDefaults standardUserDefaults] setObject:[dicBody objectForKey:@"iat"] forKey:@"iat"];
            [[NSUserDefaults standardUserDefaults] setObject:[dicBody objectForKey:@"iss"] forKey:@"iss"];
            [[NSUserDefaults standardUserDefaults] setObject:[dicBody objectForKey:@"nonce"] forKey:@"nonce"];
            [[NSUserDefaults standardUserDefaults] setObject:[dicBody objectForKey:@"sub"] forKey:@"sub"];
            [[NSUserDefaults standardUserDefaults] setObject:[dicBody objectForKey:@"tm_role"] forKey:@"roles"];
            [[NSUserDefaults standardUserDefaults] setObject:jwt forKey:@"token"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSDictionary *dicStatus = @{@"status":@"200"};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TokenResult" object:dicStatus];
        }
    }];
    [task resume];
}

- (NSString *)randomStringWithLength:(int)length{
    NSString *lettersAndNums = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
    for(int i = 0; i < length; i++){
        [randomString appendFormat:@"%C", [lettersAndNums characterAtIndex:arc4random_uniform([lettersAndNums length])]];
    }
    return randomString;
}

@end
