//
//  VCMain.h
//  quicktpms-iphone
//
//  Created by Scott Holliday on 11/3/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "DBModule.h"
#import "HasTPMSModule.h"
#import "TokenModule.h"
#import "RetrievalModule.h"

@interface VCMain : UIViewController<AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate, NSXMLParserDelegate>{
    IBOutlet UITextField *txtVIN;
    IBOutlet UIPickerView *pck;
    IBOutlet UIButton *btnRetrieve;
    IBOutlet UIButton *btnGoToTPMS;
    IBOutlet UIView *vLoading;
    UIButton *btnCancelScan;
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *videoLayer;
    NSString *selectedYear;
    Make *selectedMake;
    Model *selectedModel;
    NSArray *years;
    NSArray *makes;
    NSArray *models;
    BOOL byVIN;
    NSString *currElement;
    BOOL hasTPMS;
    NSString *sensorType;
    NSString *year;
    NSString *make;
    NSString *model;
    NSString *vin;
}

- (IBAction)scanVIN;
- (void)cancelScan;
- (IBAction)retrieveTPMSInfo;
- (IBAction)openTPMSManager;

@end
