//
//  VCMain.m
//  quicktpms-iphone
//
//  Created by Scott Holliday on 11/3/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCMain.h"

@interface VCMain ()

@end

@implementation VCMain

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenResult:) name:@"TokenResult" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retrievalResult:) name:@"RetrievalResult" object:nil];
    [[btnRetrieve layer] setCornerRadius:10];
    [btnRetrieve setClipsToBounds:YES];
    [[btnGoToTPMS layer] setCornerRadius:10];
    [btnGoToTPMS setClipsToBounds:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self clearPicker];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self retrieveTPMSInfo];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (void)clearPicker{
    selectedYear = nil;
    selectedMake = nil;
    selectedModel = nil;
    [pck reloadAllComponents];
}

- (IBAction)scanVIN{
    [txtVIN resignFirstResponder];
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if(!input){
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your device does not support VIN scan." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }else{
        [vLoading setHidden:NO];
        session = [[AVCaptureSession alloc] init];
        [session addInput:input];
        AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
        [session addOutput:captureMetadataOutput];
        dispatch_queue_t dispatch = dispatch_queue_create("queue", NULL);
        [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatch];
        [captureMetadataOutput setMetadataObjectTypes:[captureMetadataOutput availableMetadataObjectTypes]];
        videoLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
        [videoLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [videoLayer setFrame:self.view.frame];
        btnCancelScan = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnCancelScan setBackgroundImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
        [btnCancelScan setFrame:CGRectMake((self.view.frame.size.width/2)-20, self.view.frame.size.height-50, 40, 40)];
        [btnCancelScan addTarget:self action:@selector(cancelScan) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnCancelScan];
        [self.view.layer insertSublayer:videoLayer below:btnCancelScan.layer];
        [session startRunning];
    }
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if(metadataObjects != nil && [metadataObjects count] > 0){
        [vLoading setHidden:YES];
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        [txtVIN performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
        [self cancelScan];
        [self clearPicker];
    }
}

- (void)cancelScan{
    [btnCancelScan removeFromSuperview];
    [session stopRunning];
    [videoLayer removeFromSuperlayer];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    int count = 0;
    switch(component){
        case 0:
            years = [[DBModule sharedInstance] getAllYears];
            count = (int)[years count];
            break;
        case 1:
            if(selectedYear > 0){
                makes = [[DBModule sharedInstance] getMakesByYear:selectedYear];
                count = (int)[makes count];
            }
            break;
        case 2:
            if(selectedMake){
                models = [[DBModule sharedInstance] getModelsByYearAndMake:selectedYear mke:selectedMake];
                count = (int)[models count];
            }
            break;
    }
    return count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSString *title;
    switch(component){
        case 0:
            if([[years objectAtIndex:row] isEqual:[NSNumber numberWithInt:0]]){
                title = @"";
            }else{
                title = [[years objectAtIndex:row] stringValue];
            }
            break;
        case 1:{
            Make *make = [makes objectAtIndex:row];
            title = [make name];
        }
            break;
        case 2:{
            Model *model = [models objectAtIndex:row];
            title = [model name];
        }
            break;
    }
    UILabel *lbl = [[UILabel alloc] init];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [lbl setFont:[UIFont systemFontOfSize:30]];
    }else{
        [lbl setFont:[UIFont systemFontOfSize:14]];
    }
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setText:title];
    return lbl;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [txtVIN setText:@""];
    [txtVIN resignFirstResponder];
    switch(component){
        case 0:
            selectedYear = [years objectAtIndex:row];
            makes = [[DBModule sharedInstance] getMakesByYear:selectedYear];
            if([pickerView selectedRowInComponent:1] < [makes count]){
                selectedMake = [makes objectAtIndex:[pickerView selectedRowInComponent:1]];
            }else{
                selectedMake = [makes lastObject];
            }
            models = [[DBModule sharedInstance] getModelsByYearAndMake:selectedYear mke:selectedMake];
            if([pickerView selectedRowInComponent:2] < [models count]){
                selectedModel = [models objectAtIndex:[pickerView selectedRowInComponent:2]];
            }else{
                selectedModel = [models lastObject];
            }
            [pck reloadAllComponents];
            break;
        case 1:
            selectedMake = [makes objectAtIndex:row];
            [pck reloadAllComponents];
            break;
        case 2:
            selectedModel = [models objectAtIndex:row];
            break;
    }
}

- (IBAction)retrieveTPMSInfo{
    if([[txtVIN text] length] > 0){
        byVIN = YES;
        [[NSUserDefaults standardUserDefaults] setObject:[txtVIN text] forKey:@"vin"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self getHasTPMS:YES];
    }else{
        if(selectedYear && selectedMake && selectedModel){
            byVIN = NO;
            [[NSUserDefaults standardUserDefaults] setObject:selectedYear forKey:@"year"];
            [[NSUserDefaults standardUserDefaults] setObject:[selectedMake name] forKey:@"make"];
            [[NSUserDefaults standardUserDefaults] setObject:[selectedModel name] forKey:@"model"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self getHasTPMS:NO];
        }else{
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please enter or scan a VIN, or select Year, Make, and Model." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
        }
    }
}

- (void)getHasTPMS:(BOOL)byVIN{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[HasTPMSModule sharedInstance] hasTPMS:byVIN];
        dispatch_async(dispatch_get_main_queue(), ^{
            [vLoading setHidden:NO];
        });
    });
}

/*
- (void)getToken{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[TokenModule sharedInstance] getToken];
        dispatch_async(dispatch_get_main_queue(), ^{
            [vLoading setHidden:NO];
        });
    });
}

- (void)tokenResult:(NSNotification *)notification{
    NSString *status = [[notification object] objectForKey:@"status"];
    if([status isEqualToString:@"200"]){
        [[RetrievalModule sharedInstance] getTPMS:byVIN];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error retrieving your token. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [vLoading setHidden:YES];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}
*/

- (void)retrievalResult:(NSNotification *)notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *status = [[notification object] objectForKey:@"status"];
        if([status isEqualToString:@"200"]){
            NSData *data = [[notification object] objectForKey:@"data"];
            NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
            [parser setDelegate:self];
            [parser parse];
        }else{
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error retrieving your TPMS information. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
                [vLoading setHidden:YES];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
        }
    });
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict{
    currElement = elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if([currElement isEqualToString:@"a:HasTPMS"]){
        if([string isEqualToString:@"Y"]){
            hasTPMS = YES;
        }else{
            hasTPMS = NO;
        }
    }else if([currElement isEqualToString:@"a:OESensor"]){
        sensorType = string;
    }else if([currElement isEqualToString:@"a:Year"]){
        year = string;
    }else if([currElement isEqualToString:@"a:Make"]){
        make = string;
    }else if([currElement isEqualToString:@"a:Model"]){
        model = string;
    }else if([currElement isEqualToString:@"a:VIN"]){
        vin = string;
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    if(!vin){
        vin = @"Not provided";
    }
    NSString *msg;
    if(hasTPMS){
        msg = [NSString stringWithFormat:@"%@ %@ %@\nVIN: %@\nTPMS: YES\nSensor - %@", year, make, model, vin, sensorType];
    }else{
        msg = [NSString stringWithFormat:@"%@ %@ %@\nVIN: %@\nTPMS: NO", year, make, model, vin];
    }
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"TPMS Info" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
        [vLoading setHidden:YES];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    
}

- (IBAction)openTPMSManager{
    NSURL *webUrl = [NSURL URLWithString:@"http://www.tpmsmanager.com"];
    [[UIApplication sharedApplication] openURL:webUrl];
}

@end
